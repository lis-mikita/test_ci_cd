﻿namespace test_ci_cd.Tests;

public class Tests
{
    [Fact]
    public void Test1()
    {
        // Arrange
        int a = 5, b = 5;
        int expected = 10;
        // Act
        var result = a + b;
        // Accert
        Assert.Equal(expected, result);
    }

	[Fact]
	public void Test2()
	{
		// Arrange
		int a = 5, b = 4;
		int expected = 9;
		// Act
		var result = a + b;
		// Accert
		Assert.Equal(expected, result);
	}
}
